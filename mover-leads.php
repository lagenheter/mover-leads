<?php
/**
 * Plugin Name:     jimmitjoo Mover Leads
 * Plugin URI:      https://lagenheter24.se
 * Description:     A plugin to get leads for movers.
 * Author:          Jimmie Johansson
 * Author URI:      https://lagenheter24.se
 * Text Domain:     mover-leads
 * Domain Path:     /languages
 * Version:         1.0.5
 *
 * @package         Properties
 */

// Your code starts here.

function get_mover_leadsapi_root(): string
{
	$rootApiUri = env('PLOMMON_PROPERTIES_URL');

	return $rootApiUri;
}

function display_mover_leads_form($attr = [])
{
	if (isset($attr['town'])) {
		$town = $attr['town'];
	}

	$randomId = rand(1,999999);

	?>
	<div class="mover-leads-container" id="random<?=$randomId?>">
		<h4>Få gratis offerter på flytthjälp</h4>
		<form action="#" class="row">
			<div class="col-sm-6">
				<h5>Flytta från</h5>

				<div class="form-group">
					<select name="from_town" class="form-control chosen-select">
						<option value="<?= $town ?>>"><?= $town ?></option>
						<?php foreach (get_towns() as $ort) { ?>
							<option value="<?php echo $ort ?>"><?php echo $ort ?></option>
						<?php } ?>
					</select>
				</div>

				<div class="form-group">
					<input name="from_address" class="form-control" type="text" placeholder="Gatuadress">
				</div>

				<div class="form-group">
					<label for="from_type">Flyttar från</label>
					<select class="form-control" id="from_type">
						<option value="lägenhet">Lägenhet</option>
						<option value="förråd">Förråd</option>
						<option value="kontor">Kontor</option>
						<option value="radhus">Radhus</option>
						<option value="villa">Villa</option>
						<option value="övrigt">Övrigt</option>
					</select>
				</div>

				<div class="form-group">
					<input name="from_floor" class="form-control" type="number" placeholder="Våning">
				</div>

				<div class="form-group">
					<input name="from_rooms" class="form-control" type="number" placeholder="Antal rum">
				</div>

				<div class="form-group">
					<div class="input-group mb-2">
						<input name="from_area" class="form-control" type="number" placeholder="Yta">
						<div class="input-group-append">
							<div class="input-group-text">m<sup>2</sup></div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="form-check">
						<input name="from_elevator" id="from_elevator" class="form-check-input" type="checkbox"
							   value="1">
						<label class="form-check-label" for="from_elevator">
							Hiss finns!
						</label>
					</div>
				</div>

				<div class="form-group">
					<div class="form-check">
						<input name="from_addon" id="from_addon" class="form-check-input" type="checkbox" value="1">
						<label class="form-check-label" for="from_addon">
							Flytta också förråd/vind/källare
						</label>
					</div>
				</div>

				<div class="form-group">
					<div class="form-check">
						<input name="from_packing" id="from_packing" class="form-check-input" type="checkbox"
							   value="1">
						<label class="form-check-label" for="from_packing">
							Önskar hjälp med packning
						</label>
					</div>
				</div>

				<div class="form-group">
					<div class="form-check">
						<input name="from_cleaning" id="from_cleaning" class="form-check-input" type="checkbox"
							   value="1">
						<label class="form-check-label" for="from_cleaning">
							Önskar hjälp med flyttstädning
						</label>
					</div>
				</div>

				<div class="form-group">
					<div class="form-check">
						<input name="from_moving_box" id="from_moving_box" class="form-check-input" type="checkbox"
							   value="1">
						<label class="form-check-label" for="from_moving_box">
							Behöver flyttkartonger
						</label>
					</div>
				</div>

				<div class="form-group">
					<div class="form-check">
						<input name="from_warehousing" id="from_warehousing" class="form-check-input"
							   type="checkbox" value="1">
						<label class="form-check-label" for="from_warehousing">
							Önskar magasinering
						</label>
					</div>
				</div>

			</div>
			<div class="col-sm-6">
				<h5>Flytta till</h5>

				<div class="form-group">
					<select name="to_town" class="chosen-select">
						<option value="<?= $town ?>>"><?= $town ?></option>
						<?php foreach (get_towns() as $ort) { ?>
							<option value="<?php echo $ort ?>"><?php echo $ort ?></option>
						<?php } ?>
					</select>
				</div>

				<div class="form-group">
					<input name="to_address" class="form-control" type="text" placeholder="Gatuadress">
				</div>

				<div class="form-group">
					<label for="to_type">Flyttar till</label>
					<select class="form-control" id="to_type">
						<option value="lägenhet">Lägenhet</option>
						<option value="förråd">Förråd</option>
						<option value="kontor">Kontor</option>
						<option value="radhus">Radhus</option>
						<option value="villa">Villa</option>
						<option value="övrigt">Övrigt</option>
					</select>
				</div>

				<div class="form-group">
					<input name="to_floor" class="form-control" type="number" placeholder="Våning">
				</div>

				<div class="form-group">
					<input name="to_rooms" class="form-control" type="number" placeholder="Antal rum">
				</div>

				<div class="form-group">
					<div class="input-group mb-2">
						<input name="to_area" class="form-control" type="number" placeholder="Yta">
						<div class="input-group-append">
							<div class="input-group-text">m<sup>2</sup></div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="form-check">
						<input name="to_elevator" id="to_elevator" class="form-check-input" type="checkbox"
							   value="1">
						<label class="form-check-label" for="to_elevator">
							Hiss finns!
						</label>
					</div>
				</div>

				<div class="form-group">
					<div class="form-check">
						<input name="to_packing" id="to_packing" class="form-check-input" type="checkbox" value="1">
						<label class="form-check-label" for="to_packing">
							Önskar hjälp med uppackning
						</label>
					</div>
				</div>

			</div>
			<div class="col-sm-12">
				<hr>
				<div class="form-group">
                        <textarea class="form-control"
								  placeholder="Övrig information. En bra beskrivning gör det enklare att ge dig rätt pris!"></textarea>
				</div>
			</div>
			<div class="col-sm-12">

				<div class="form-group">
					<div class="form-check">
						<input name="terms" id="terms" class="form-check-input" type="checkbox" value="1" required>
						<label class="form-check-label" for="terms">
							<small>Jag godkänner villkoren för offertförfrågan. Tjänsten är helt gratis i hela
								Sverige.</small>
						</label>
					</div>
				</div>

				<div class="form-group">
					<button class="btn btn-primary">Skapa gratis förfrågan</button>
				</div>
			</div>
		</form>
		<div class="gradient"></div>
		<button class="expander-btn" onclick="btnClick('random<?=$randomId?>')">Visa mer</button>
	</div>
	<?php
}
add_shortcode('mover_leads', 'display_mover_leads_form');


function mover_leads_styles() {
	wp_enqueue_style(
		'mover-leads-style',
		plugins_url('', __FILE__) . '/mover-leads.css',
		[]
	);
}
add_action('wp_enqueue_scripts', 'mover_leads_styles');

function mover_leads_scripts() {
	wp_enqueue_script(
		'mover-leads-script',
		plugins_url('', __FILE__) . '/mover-leads.js',
		['jquery']
	);
}
add_action('wp_enqueue_scripts', 'mover_leads_scripts');
